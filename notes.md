# World

## Names

- there's somewhat middle option: they all have names, but they introduce
  themselves using titles, because "NOBODY SHOULD KNOW YOUR TRUE NAME" or just
  because it's considered familiar or something

- True Names: do they have meaning? perhaps

- we could have something that translates to " I am iron given purpose to give
  purpose. I have a heart of valves and hands of hinged metal. I walk on burning
  coals and breath steam." a sort of "I Am"-"I have"-"I do" structure

## Country

- generic name, like Empire or something

- Generica, land of the brave

## Daemons

- what else are demons used for? we've got 1) power source for trains

- they could be used on factories

- messenger demons, military demons, tinker demons?

- since they exist in free-floating form, if you can control them, you can make
  them do fancy things

- I've also considered that demons could just be disembodied human spirits that
  have been enslaved / conditioned / processed

- but nobody knows about it until the terrible truth is revealed or it's
  everyday fact nobody cares about?

- actually public knowledge could be cool for the setting. It's a controversial
  idea that can inspire different factions and philosophies, and basically means
  it's steampunk necromancy 

- well, that's pretty much the question of "evil gov &/ corps" vs "indifferent
  citizens". if that's condsidered evil, that is

- well, the spirits of the dead are being used as power sources, they retain
  sentience (but we have no evident that they retain memory), they probably
  don't feel physical pain. I think economic forces would encourage demon use,
  especially if they could be processed to be less human.

- there's the question: why not fully resurrect them?

- we can also add some sects opposing it because "it's immoral to disturb
  spirits of dead"

- maybe they can't? just because you have a soul doesn't mean you can make a
  body for it.

- then you introduce a character who thinks they can raise the dead and make
  them a horribly disturbing serial killer

- a sort of Frankenstein government conspiracy? May or may not be true?

- there's also another twist possibility: "daemons are actually not made of dead
  souls, but nobody knows it. in reality, those daemons are just attracted to
  the place of death and can be thus can be captured"

- it can actually mean many different things. most dramatically: normally,
  daemons are fetching souls after death and taking them into underworld to put
  them into soul reincarnation conveyer. when you catch the daemon, soul rots in
  neitherworld and either dies forever or becomes horrible soulmonster

- so greedy corporations are killing soul ecosystem and the world needs to be
  saved again

- HCDC = high compression daemon container

### Daemon types

- celestial: very powerful and hard to control. see chapter two

- fire daemons - what are other types?

## Technology

- I imagine the ability to use demons to heat water removes much of the
  disadvantages with steampower (demonology replaces coal), so steam takes over
  everything and other technologies are underdeveloped

- mobile transport: compact demon engines & horses

- A charles baggage engine with cards and vacuum tubes would be a super
  computer.


# Characters

## Conductor

- As soon as the train arrives at it's ultimate destination, Conductor is fired
  for being 36 minutes late.

- is the conductor the kind of guy who would jump into something illegal? More
  fun if he's not, but harder to write

- he seems to be pretty loyal by default, but he's angry at his company and have
  no job

- maybe it would make sense to make him angry quit, spend a day searching job
  and a night in a garbage and then accept the plan

## Detective

- "Detective" is pulling out different shady projects, more or less criminal.
  this time he's assembling team for a really ambitious job, part of the plan is
  using daemons, that's why he needs someone who works with them.

## Barman

- acquainted with Detective

## Detective's "supervisor" (??)

- see detective.txt

## Detective's Crew

- The Critic

- creepy serial killer person, who is very ordinary but happens to enjoy killing
  and torture. Very laid back personality, but craves intense situations.

# Events

## Detective's plan

- conductor is required to control daemons

- hmm, maybe sneak the conductor into the engine, force the train to make a
  wrong turn into another town, take whatever cargo you want and run, lead the
  train back onto it's main track. But makes the conductor a victim

- use daemons to sneak into vault

- More likely there's a specific cargo that you want and a deal in advance.
  Maybe raw supplies stolen from a rival company?

- unless it's something completely different, or one step in a more personal
  plan  

- detective won't tell details at this point anyway, especially since he'll try
  to cover things up to look less criminal for conductor

- we need a multi level plan:
    1. what the detective tells the conductor
    2. what they're really doing
    3. what the detective really wants
    4. what they lead up to

- anything like jumping on to a moving train is too obvious. Breaking in to a
  building can be done if the detective has a key, or bluffed if he says "dude
  gave me the wrong key" and breaks a window. 

- it could be after money (kinda boring on it's own, unless put in interesting
  way), some kind of personal revenge (nice working cliche), just for fun (could
  be hard to make it work) or actually as part of plan to save the world
  (perhaps too pompous to put it straight away)
